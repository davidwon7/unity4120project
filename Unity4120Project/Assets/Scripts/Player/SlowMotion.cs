﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Slow motion script in C# by LEUNG Chun Kit Daniel (1155047572)
// Usage: Put it as a child under the hierachy of the player, set boolean of SlowNow to true to use
// Tested 8-3-2017



public class SlowMotion : MonoBehaviour {

    // public boolean to determine if SlowMo is active
    public bool SlowNow;
    float SlowMo = 0.1f;
    float Realtime = 1.0f;
    bool trigger;
 
	
	// Update is called once per frame
	void Update () {
        if (SlowNow == true)
            trigger = true;
        else
            trigger = false;

        if (Input.GetKeyDown("w")|| Input.GetKeyDown("s")|| Input.GetKeyDown("a")|| Input.GetKeyDown("d"))
        {
            // 
            ResetEffect();
        }
    }

    // Fixed update for Physics related components
    void FixedUpdate()
    {
        if (trigger)
        {
            if (Input.GetKeyUp("w") || Input.GetKeyUp("s") || Input.GetKeyUp("a") || Input.GetKeyUp("d"))
            {
                // Activate slowmotion
                // Will insert Sound effect / other function calls later
                SlowEffect();
            }
        }
    }

    void SlowEffect()
    {
        Time.timeScale = SlowMo;
        Time.fixedDeltaTime = (float)0.02 * Time.timeScale;
    }

    void ResetEffect()
    {
        Time.timeScale = Realtime;
        Time.fixedDeltaTime = (float)0.02 * Time.timeScale;
    }
}
