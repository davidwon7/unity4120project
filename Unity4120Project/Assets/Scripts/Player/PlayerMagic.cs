﻿using UnityEngine;


// Shooting out Magic sprays that dmg enemy
public class PlayerMagic : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.01f;

    float timer;
  
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem magicParticles;
    Collider sprayHit;

    float effectsDisplayTime = 0.2f;


    void Awake()
    {
        shootableMask = LayerMask.GetMask("Shootable");
        magicParticles = GetComponent<ParticleSystem>();
        sprayHit = GetComponent<BoxCollider>();
    }


    void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot();
        }

        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects();
        }
    }


    public void DisableEffects()
    {
        magicParticles.Stop();
        sprayHit.enabled = false;
    }


    void Shoot()
    {
        timer = 0f;
        //spray effect starts
        magicParticles.Play();
        //start detecting collision
        sprayHit.enabled=true;
    }

    // inflicts dmg
    void OnCollisiionEnter(Collision collision)
    {
        //To be implemented with Enemies
        collision.gameObject.SetActive(false);
    }
}
