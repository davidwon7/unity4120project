﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItem : MonoBehaviour {
    //access to item script
    public ItemSpawn itemScript;

    int currentItem,preItem;
    //UPDATE 2-4-2017
    GameObject gun, gunBarrel, staff;

    public GameObject[] weaponArray;

    // Use this for initialization
    void Awake () {
        //init weapons
        weaponArray = new GameObject[9];
        //gunBarrel = GameObject.Find("GunBarrelEnd");
        gun = GameObject.Find("Gun");
        weaponArray[0] = gun;
        weaponArray[1] = gun;
        staff = GameObject.Find("Staff");
        weaponArray[2] = staff;
        weaponArray[3] = gun;
        weaponArray[4] = gun;
        weaponArray[5] = gun;
        weaponArray[6] = gun;
        weaponArray[7] = gun;
        weaponArray[8] = gun;



        //default weapon is gun
        preItem = 1;
        currentItem = 1;
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //TRY 2-4-2017
        CheckItem();
    }

    //try 2-4-2017
    void CheckItem()
    {
        //item 1: Gun
        if(currentItem==1)
        {
            foreach(GameObject wp in weaponArray)
            {
                wp.SetActive(false);
            }
            gun.SetActive(true);
            //gunBarrel.SetActive(true);
        }

        //item 2:
        if (currentItem == 2)
        {
            foreach (GameObject wp in weaponArray)
            {
                wp.SetActive(false);
            }
            staff.SetActive(true);
        }

        if (Input.GetKeyDown("f"))
        {
            currentItem = 0;
            gun.SetActive(false);
            gunBarrel.SetActive(false);
        }
    }

    // Pick up Weapon
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Weapon"))
        {
            //Switch weapon
            //TRY: currently disable gun
            // Hide the crate
            other.gameObject.SetActive(false);
            Destroy(other.gameObject, 0);
            itemScript.itemCount -= 1;
            //generate random weapon
            randomWeapon();
        }
    }

    void randomWeapon()
    {
        //randomise a number
        currentItem = (int)(Random.value*10);
        //if same, +1 to result
        if (currentItem == preItem)
            currentItem++;
        Debug.Log("generated" + currentItem );
        preItem = currentItem;
    }
}
