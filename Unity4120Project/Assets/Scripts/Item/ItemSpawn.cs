﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour {

    public int itemCount;
    public GameObject item;
    public float spawnTime = 2f;
    public Transform[] spawnPoints;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("SpawnItem", spawnTime, spawnTime);
		
	}
	
	// Update is called once per frame
	void SpawnItem () {
        if(itemCount>=1)
        {
            return;
        }

        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        GameObject clone;
        clone=Instantiate(item, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        clone.SetActive(true);
        itemCount += 1;
		
	}
}
