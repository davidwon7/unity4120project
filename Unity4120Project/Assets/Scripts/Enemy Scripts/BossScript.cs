﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; 

public class BossScript : MonoBehaviour {
    public GameObject[] splitEnemies;
    private float moveSpeed;                 //Movespeed of enemy 
    private EnemyStatsScript stats;          //Enemy stats 
    private GameObject player;              //Player 
    private NavMeshAgent nav;               //Navigation mesh 
    public GameObject Bullet;               //Bullet for ranged enemies 
    private float fireTimer;                //For keeping track of fire time 
    public float fireInterval;              //Fire a bullet every X seconds 


    // Use this for initialization
    void Start () {
        stats = GetComponent<EnemyStatsScript>();
        moveSpeed = stats.moveSpeed;
        player = GameObject.FindGameObjectWithTag("Player");
        nav = GetComponent<NavMeshAgent>();
        fireTimer = fireInterval;
    }
	
	// Update is called once per frame
	void Update () {
        MeleeScript();
        BossAI(); 

        if(transform.localScale.y < 10)
        {
            Destroy(this.gameObject);
        }

        if(stats.actualHP <= 0)
        {
            stats.RestoreHP();
            //Split into two smalller bosses 
            for(int i = 0; i < 2; i++)
            {
                GameObject halfBoss = Instantiate(this.gameObject, transform.position, transform.rotation);
                halfBoss.transform.localScale *= 0.5f;
                halfBoss.GetComponent<EnemyStatsScript>().hitPoints = stats.hitPoints/2;
                halfBoss.GetComponent<EnemyStatsScript>().moveSpeed = stats.moveSpeed * 1.5f;
                halfBoss.transform.position = new Vector3(transform.position.x, transform.position.y / 2, transform.position.z);
            }
            Destroy(this.gameObject);
        }
	}

    void BossAI()
    {
        Vector3 rayDirection = player.transform.position - transform.position;
        if (PlayerInSight(rayDirection))
        {
            FireBullet(rayDirection);
            MeleeScript();
        }
        else
        {
            MeleeScript();
        }
    }

    void MeleeScript()
    {
        nav.SetDestination(player.transform.position);
        nav.speed = moveSpeed;
    }

    void RangedScript()
    {
        Vector3 rayDirection = player.transform.position - transform.position;
        if (PlayerInSight(rayDirection))
        {
            FireBullet(rayDirection);
            StopMovement();
        }
        else
        {
            MeleeScript();
        }
    }

    bool PlayerInSight(Vector3 rayDirection)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, rayDirection, out hit))
        {
            if (hit.transform.tag == "Player" || hit.transform.tag == "Bullet")
            {
                return true;
            }
        }
        return false;
    }

    void FireBullet(Vector3 playerDirection)
    {
        fireTimer -= Time.deltaTime;
        if (fireTimer <= 0)
        {
            //Fire the bullet 
            GameObject attack = Instantiate(Bullet, transform.position, transform.rotation);
            attack.GetComponent<Rigidbody>().velocity = new Vector3(playerDirection.x, 0, playerDirection.z);

            //Reset the fire timer 
            fireTimer = fireInterval;
        }
    }

    void StopMovement()
    {
        nav.SetDestination(transform.position);
        nav.speed = 0;
    }
}
