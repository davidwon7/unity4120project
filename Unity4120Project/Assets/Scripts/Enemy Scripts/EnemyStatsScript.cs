﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStatsScript : MonoBehaviour {

    public float moveSpeed;
    public float attackDmg;
    public float hitPoints;
    public float actualHP; 
    public bool isRanged;
    public bool isBoss;

	// Use this for initialization
	void Start () {
        actualHP = hitPoints;
	}
    
	
	// Update is called once per frame
	void Update () { 

        //No more hitpoints 
		if(actualHP <= 0 && !isBoss)
        {
            Destroy(this.gameObject); 
        }
	}


    void OnCollisionEnter(Collision coll)
    {
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.transform.tag == "Weapon")
        {
            actualHP -= 1;
        }
    }

    public void RestoreHP()
    {
        actualHP = hitPoints;  
    }
}
