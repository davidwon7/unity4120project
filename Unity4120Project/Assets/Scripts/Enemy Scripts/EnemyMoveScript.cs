﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMoveScript : MonoBehaviour {

    private float moveSpeed;                 //Movespeed of enemy 
    private EnemyStatsScript stats;          //Enemy stats 
    private GameObject player;              //Player 
    private NavMeshAgent nav;               //Navigation mesh 
    public GameObject Bullet;               //Bullet for ranged enemies 
    private float fireTimer;                //For keeping track of fire time 
    public float fireInterval;              //Fire a bullet every X seconds 


    // Use this for initialization
    void Start() {
        stats = GetComponent<EnemyStatsScript>();
        moveSpeed = stats.moveSpeed;
        player = GameObject.FindGameObjectWithTag("Player");
        nav = GetComponent<NavMeshAgent>();
        fireTimer = fireInterval;
    }

    // Update is called once per frame
    void Update() {
        if (stats.isRanged)
        {
            RangedScript();
        }
        else {
            MeleeScript();
        }
    }

    void MeleeScript()
    {
        nav.SetDestination(player.transform.position);
        nav.speed = moveSpeed;
    }

    void RangedScript()
    {
        Vector3 rayDirection = player.transform.position - transform.position;
        if (PlayerInSight(rayDirection))
        {
            FireBullet(rayDirection);
            StopMovement();
        }
        else
        {
            MeleeScript();
        }
    }    

    bool PlayerInSight(Vector3 rayDirection)
    {        
        RaycastHit hit;
        if (Physics.Raycast(transform.position, rayDirection, out hit))
        {            
            if (hit.transform.tag == "Player" || hit.transform.tag == "Bullet")
            {
                return true;
            }                    
        }
        return false;
    }

    void FireBullet(Vector3 playerDirection)
    {
        fireTimer -= Time.deltaTime;
        if (fireTimer <= 0)
        {
            //Fire the bullet 
            GameObject attack = Instantiate(Bullet, transform.position, transform.rotation);
            attack.GetComponent<Rigidbody>().velocity = new Vector3(playerDirection.x, 0, playerDirection.z);

            //Reset the fire timer 
            fireTimer = fireInterval;
        }
    }

    void StopMovement()
    {        
        nav.SetDestination(transform.position);
        nav.speed = 0;
    }
}
